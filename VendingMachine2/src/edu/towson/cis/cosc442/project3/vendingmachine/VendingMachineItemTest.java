/**
 * 
 */
package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 */
public class VendingMachineItemTest {

	/**
	 * @throws java.lang.Exception
	 */
	
	VendingMachine myMachine;
	VendingMachineItem soda;
	
	@Before
	public void setUp() throws Exception {
		soda = new VendingMachineItem("soda", 3.0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		soda = null;
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachineItem#VendingMachineItem(java.lang.String, double)}.
	 */
	@Test
	public void testVendingMachineItem() {
		VendingMachineItem chips;
		chips = new VendingMachineItem("chips", 4.0);
	}


	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachineItem#getName()}.
	 */
	@Test
	public void testGetName() {
		assertTrue(soda.getName().equals("soda"));
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachineItem#getPrice()}.
	 */
	@Test
	public void testGetPrice() {
		assertEquals(3.0, soda.getPrice(), 0.001);
	}

}
