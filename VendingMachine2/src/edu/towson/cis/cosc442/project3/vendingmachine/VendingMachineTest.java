/**
 * 
 */
package edu.towson.cis.cosc442.project3.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 */
public class VendingMachineTest {

	/**
	 * @throws java.lang.Exception
	 */
	
	VendingMachine myMachine;
	VendingMachineItem soda;
	VendingMachineItem chips;
	
	@Before
	public void setUp() throws Exception {
		myMachine = new VendingMachine();
		soda = new VendingMachineItem("soda", 3.0);
		chips = new VendingMachineItem("chips", 3.0);
		myMachine.addItem(chips, "D");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#VendingMachine()}.
	 */
	@Test
	public void testVendingMachine() {
		
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#addItem(edu.towson.cis.cosc442.project3.vendingmachine.VendingMachineItem, java.lang.String)}.
	 */
	@Test
	public void testAddItem() {
		//tests inserting item into empty space
		myMachine.addItem(soda, "A");
		assertTrue(myMachine.getItem("A").equals(soda));
		
		
	}
	
	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#addItem(edu.towson.cis.cosc442.project3.vendingmachine.VendingMachineItem, java.lang.String)}.
	 */
	@Test
	public void testAddItemException() throws VendingMachineException {
		//tests inserting item into occupied slot
		myMachine.addItem(soda, "D");
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#getItem(java.lang.String)}.
	 */
	@Test
	public void testGetItem() {
		//tests getting item from occupied slot
		myMachine.addItem(soda, "A");
		assertEquals(soda, myMachine.getItem("A"));
		
		//tests trying to get item from empty slot
		assertEquals(null, myMachine.getItem("B"));
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#removeItem(java.lang.String)}.
	 */
	@Test
	public void testRemoveItem() {
		//tests that slot is empty after item is removed
		myMachine.addItem(soda, "A");
		myMachine.removeItem("A");
		assertEquals(null, myMachine.getItem("A"));
	}
	
	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#removeItem(java.lang.String)}.
	 */
	@Test
	public void testRemoveItemException() throws VendingMachineException {
		//tests removing item from occupied slot
		myMachine.removeItem("C");
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#insertMoney(double)}.
	 */
	@Test
	public void testInsertMoney() {
		//tests that money was input correctly
		myMachine.insertMoney(3.0);
		assertEquals(3.0, myMachine.getBalance(), 0.001);
	}
	
	@Test
	public void testInsertMoneyException() throws VendingMachineException {
		//tests for negative inputs
		myMachine.insertMoney(-1.0);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#getBalance()}.
	 */
	@Test
	public void testGetBalance() {
		myMachine.insertMoney(5.0);
		assertEquals(5.0, myMachine.getBalance(), 0.001);
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#makePurchase(java.lang.String)}.
	 */
	@Test
	public void testMakePurchase() {
		//makes sure method returns "true" when both conditions are met
		myMachine.addItem(soda, "A");
		myMachine.insertMoney(5.0);
		assertTrue(myMachine.makePurchase("A"));
		
		//makes sure method returns "false" when neither condition is met
		assertFalse(myMachine.makePurchase("C"));
		
		//makes sure method returns "false" when only one condition is met
		myMachine.addItem(chips, "B");
		assertFalse(myMachine.makePurchase("B"));
	}

	/**
	 * Test method for {@link edu.towson.cis.cosc442.project3.vendingmachine.VendingMachine#returnChange()}.
	 */
	@Test
	public void testReturnChange() {
		//makes sure correct change value is returned after purchase
		myMachine.addItem(soda, "A");
		myMachine.insertMoney(6.0);
		myMachine.makePurchase("A");
		assertEquals(3.0, myMachine.returnChange(), 0.001);
	}

}
